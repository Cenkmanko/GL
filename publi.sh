#!/bin/bash

STYLESHEET="rubygems"
STYLESDIR="stylesheets"
IMAGESDIR="resources"
EXPORTDIR="public"

# compile sassy stylesheets (committed in ./sass folder)
# into CSS stylesheets which will be created in $STYLESDIR
# tose locations are set in ./config.rb
compass compile
# we have to move our stylesheet in the execution folder
cp ${STYLESDIR}/${STYLESHEET}.css .
# don't forget to copy resources, too
mkdir -p ${EXPORTDIR}
cp -r course/resources ${EXPORTDIR}/
cp -r practical/resources ${EXPORTDIR}/

###################
# COURSE MATERIAL #
###################
LOCALE="fr"
# generate .html page in output folder
asciidoctor course/genie_logiciel.adoc \
         -a stylesheet="${STYLESHEET}.css"    \
         -a stylesdir="../${STYLESDIR}"       \
         -a imagesdir="../${IMAGESDIR}"       \
         -o ${EXPORTDIR}/${LOCALE}/index.html
# generate .pdf document in output folder
wkhtmltopdf --enable-internal-links --enable-external-links \
            ${EXPORTDIR}/${LOCALE}/index.html \
            ${EXPORTDIR}/${LOCALE}/pdf
# generate .epub document in output folder
asciidoctor-epub3 course/genie_logiciel.adoc \
         -a imagesdir="${IMAGESDIR}"         \
         -D .
mv genie_logiciel.epub ${EXPORTDIR}/${LOCALE}/RégisWITZ.epub

LOCALE="en"
# generate .html page in output folder
asciidoctor course/software_engineering.adoc \
         -a stylesheet="${STYLESHEET}.css"    \
         -a stylesdir="../${STYLESDIR}"       \
         -a imagesdir="../${IMAGESDIR}"       \
         -o ${EXPORTDIR}/${LOCALE}/index.html
# generate .pdf document in output folder
wkhtmltopdf --enable-internal-links --enable-external-links \
            ${EXPORTDIR}/${LOCALE}/index.html \
            ${EXPORTDIR}/${LOCALE}/pdf
# generate .epub document in output folder
asciidoctor-epub3 course/software_engineering.adoc \
         -a imagesdir="${IMAGESDIR}"               \
         -D .
mv software_engineering.epub ${EXPORTDIR}/${LOCALE}/RégisWITZ.epub


###################
# PRACTICAL WORKS #
###################
declare -a FILES=(
    "scm"
    "unit_testing_python" "unit_testing_java"
    "ci_doc"
    "bdd"
  )
for f in "${FILES[@]}"; do
  LOCALE="en"
  asciidoctor practical/${LOCALE}/$f.adoc  \
         -a stylesheet="${STYLESHEET}.css" \
         -a stylesdir="../../${STYLESDIR}" \
         -a imagesdir="../../${IMAGESDIR}" \
         -o ${EXPORTDIR}/${LOCALE}/$f.html
  LOCALE="fr"
  asciidoctor practical/${LOCALE}/$f.adoc  \
         -a stylesheet="${STYLESHEET}.css" \
         -a stylesdir="../../${STYLESDIR}" \
         -a imagesdir="../../${IMAGESDIR}" \
         -o ${EXPORTDIR}/${LOCALE}/$f.html
done

############
# PROJECTS #
############

declare -a FILES=(
    "project/love-letter"
    "project/codenames"
  )
for f in "${FILES[@]}"; do
  LOCALE="en"
  asciidoctor practical/${LOCALE}/$f.adoc  \
         -a stylesheet="${STYLESHEET}.css" \
         -a stylesdir="../../../${STYLESDIR}" \
         -a imagesdir="../../../${IMAGESDIR}" \
         -o ${EXPORTDIR}/${LOCALE}/$f.html
  LOCALE="fr"
  asciidoctor practical/${LOCALE}/$f.adoc  \
         -a stylesheet="${STYLESHEET}.css" \
         -a stylesdir="../../../${STYLESDIR}" \
         -a imagesdir="../../../${IMAGESDIR}" \
         -o ${EXPORTDIR}/${LOCALE}/$f.html
done
